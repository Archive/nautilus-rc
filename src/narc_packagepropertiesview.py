import narc_channel
import narc_util

from narc_channel import Channel
from narc_util import GladeWidget
from narc_util import StockImageButton
from narc_util import fault
from narc_serverproxy import ServerProxy
from narc_transactionwindow import Transaction
from narc_transactionwindow import UpdateWindow
from narc_transactionwindow import TransactionPreviewDialog

import gobject
import gtk
import gtk.glade
import string
import ximian_xmlrpclib

from gtk import TRUE, FALSE

class PackagePropertiesView (GladeWidget) :
    def __init__ (self, server) :
        GladeWidget.__init__ (self,
                              'nautilus-rc.glade',
                              'package_properties_vbox')
        
        self.server = server

        self.package_fields = {}

        self.create_size_group (['name_title',
                                 'version_title',
                                 'release_title',
                                 'section_title',
                                 'channel_title'])

        self.add_package_label ('name')
        self.add_package_label ('version')
        self.add_package_label ('release')
        self.add_package_label ('section_user_str')

        self.update_label = self.xml.get_widget ('update_label')
        self.update_button = self.xml.get_widget ('update_button')
        self.channel_image = self.xml.get_widget ('channel_image')
        self.channel_label = self.xml.get_widget ('channel_label')

        self.update_button.connect ('clicked', self.update_clicked_cb)

        self.set_package (None)

        self.uri = None

    def update_completed_cb (self, transaction) :
        if (self.uri) :
            self.set_file (self.uri)
    def dependency_response_cb (self, dialog, response, transaction) :
        dialog.destroy ()
        if (response == 1) :
            transaction.connect ('completed',
                                 self.update_completed_cb)

            window = UpdateWindow (self.get_toplevel (), transaction)
            window.show ()

            transaction.execute ()

    def update_clicked_cb (self, button) :
        # x[1] is the package to be updated
        packages_to_install = map(lambda x:x[1], self.updates)

        try :
            install_deps, remove_deps, dep_info = \
                          narc_util.resolve_dependencies (self.server,
                                                         packages_to_install, [], [])
        except ximian_xmlrpclib.Fault, f :
            narc_util.error_dialog (self, f.faultString)
            return
        except Exception, e:
            narc_util.error_dialog (self, str (e))
            return

        transaction = Transaction (self.server,
                                   packages_to_install, install_deps,
                                   [], remove_deps)

        window = TransactionPreviewDialog (self.get_toplevel (),
                                           self.server, transaction,
                                           verb='_Update')
        window.connect ('response',
                        self.dependency_response_cb,
                        transaction)

        window.show ()

    def add_package_label (self, name) :
        self.package_fields[name] = self.xml.get_widget ('%s_label' % (name))

    def set_package_labels (self) :
        for key in self.package_fields.keys () :
            if (self.package) :
                try :
                    self.package_fields[key].set_text (self.package[key])
                except KeyError :
                    self.package_fields[key].set_text ('Unknown')
            else :
                self.package_fields[key].set_text ('Unknown')

    def set_info_values (self) :
        description_text = self.xml.get_widget ('description_text')
        if (self.package) :
            description = self.info['description']
            description_text.get_buffer().set_text (description)
        else :
            description_text.get_buffer().set_text ("")
        
    def set_channel_values (self) :
        if (self.channel) :
            self.channel_label.set_text (self.channel['name'])

            width, height = gtk.icon_size_lookup (gtk.ICON_SIZE_MENU)

            pixbuf = self.channel.get_channel_icon (width, height)
            if (pixbuf) :
                self.channel_image.set_from_pixbuf (pixbuf)
            else :
                # FIXME
                pass
            self.channel_image.show ()
        else :
            self.channel_image.hide ()
            self.channel_label.set_text ('Unknown')

    def get_applicable_updates (self, updates) :
        def update_applies (update, channel_list=narc_channel.get_subscribed_channel_ids (self.server)) :
            return (update[1]['channel'] in channel_list) and (update[1]['name'] == self.package['name'])
            
        updates = filter (update_applies, updates)

        return updates

    def got_updates (self, thread) :
        updates = thread.get_result ()

        updates = self.get_applicable_updates (updates)
        
        self.updates = updates

        if len (updates) == 0 :
            self.update_label.set_text ('This package is up to date.')
            self.update_button.set_sensitive (FALSE)
        else :
            self.update_label.set_text ('There are updates available for this package.')
            self.update_button.set_sensitive (TRUE)


    def check_updates (self) :
        if not self.package :
            # FIXME - text could be better
            self.update_label.set_text ('')
            return
        
        can_update = self.server.rcd.users.has_privilege ('upgrade')

        update_box = self.xml.get_widget ('update_box')
        if can_update :
            update_box.show ()
            self.update_label.set_text ('Checking for updates...')
            self.update_button.set_sensitive (FALSE)
            
            proxy = ServerProxy (self.server)
            thread = proxy.rcd.packsys.get_updates ()
            thread.connect ('ready', self.got_updates)
        else :
            update_box.hide ()
    def set_package (self, package) :
        self.package = package

        if (package) :
            self.info = self.server.rcd.packsys.package_info (package['name'])
            if (package.has_key ('channel') and package['channel'] != 0) :
                self.channel = Channel (self.server, package['channel'])
            elif (package.has_key ('channel_guess')) :
                self.channel = Channel (self.server, package['channel_guess'])
            else :
                self.channel = None
            self.set_sensitive (TRUE)
        else :
            self.channel = None
            self.set_sensitive (TRUE)

        self.set_package_labels ()
        self.set_channel_values ()
        self.set_info_values ()

        self.check_updates ()

    def set_file (self, uri) :
        self.uri = uri

        package_name = narc_util.get_uri_package_name (self.server, self.uri)
        package = narc_util.get_installed_package (self.server, package_name)
        
        self.set_package (package)
