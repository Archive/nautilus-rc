import os
import sys

import narc_util

from narc_transactionwindow import TransactionPreviewDialog
from narc_transactionwindow import TransactionPreview
from narc_transactionwindow import Transaction
from narc_transactionwindow import InstallationWindow

from narc_util import error_dialog
from narc_util import StockImageButton
from narc_util import URINotSupported
from narc_util import URINotFound
from narc_util import fault

import ORBit
import CORBA
import bonobo
import bonobo.ui
import gobject
import gtk
import os
import socket
import ximian_xmlrpclib

from gtk import TRUE, FALSE

def dependency_response_cb (dialog, response, transaction) :
    dialog.destroy ()

    if (response == 1) :
        window = InstallationWindow (None, transaction)
        window.show ()

        transaction.execute ()

def install_files (server, files) :
    packages_to_install = []
    for file in files :
        try :
            package = server.rcd.packsys.query_file (file)
            package['package_filename'] = file
        except ximian_xmlrpclib.Fault, f :
            error_dialog (None, '%s cannot be installed' % (file))
            return
        
        packages_to_install.append (package)

    try :
        install_deps, remove_deps, dep_info = \
                      narc_util.resolve_dependencies (server,
                                                     packages_to_install, [], [])
    except ximian_xmlrpclib.Fault, f :
        narc_util.error_dialog (None, f.faultString)
        return

    transaction = Transaction (server,
                               packages_to_install,
                               install_deps,
                               [],
                               remove_deps)

    window = TransactionPreviewDialog (None, server, transaction)

    window.connect ('response',
                    dependency_response_cb,
                    transaction)

    window.show ()

def event_cb (listener, event, arg, user_data) :
    if (event == 'installpackage') :
        server = ximian_xmlrpclib.Server ("/var/run/rcd/rcd", verbose=0)
        uris = arg.value()
        files = []
        for uri in uris :
            if (uri[:7] != 'file://') :
                raise URINotSupported
            else :
                files.append (uri[7:])

        install_files (server, files)

def factory_fn(factory, iid):
    listener = bonobo.Listener (event_cb)
    return listener


def run () :
    gtk.threads_init ()
    
    bonobo.activate ()
    fact = bonobo.GenericFactory ("OAFIID:Nautilus_Install_Package_Factory",
                                  factory_fn)
    bonobo.running_context_auto_exit_unref (fact)
    
    gtk.main ()
