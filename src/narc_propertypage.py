import sys
import os

import narc_util

from narc_channel import Channel
from narc_packagepropertiesview import PackagePropertiesView
from narc_util import error_dialog

from narc_util import URINotSupported
from narc_util import URINotFound
from narc_util import fault

import ORBit
import CORBA
import bonobo
import bonobo.ui
import gobject
import gtk
import os
import socket
import ximian_xmlrpclib

from gtk import TRUE, FALSE

class PackagePropertiesControl (bonobo.ui.Control) :
    PROP_URI = 0

    def __init__ (self, server) :
        self.server = server

        self.notebook = gtk.Notebook ()

        bonobo.ui.Control.__init__ (self, self.notebook)
        
        self.notebook.set_show_tabs (FALSE)
        self.notebook.set_border_width (0)
        self.notebook.set_show_border (FALSE)

        self.error_label = gtk.Label ("This file does not belong to any package.")
        self.error_label.set_line_wrap (TRUE)
        self.error_label.show ()
        
        self.view = PackagePropertiesView (server)
        self.view.show ()

        self.notebook.append_page (self.view, gtk.Label (''))
        self.notebook.append_page (self.error_label, gtk.Label (''))
        self.notebook.show ()

        bag = bonobo.PropertyBag (self.get_prop, self.set_prop)

        self.uri_type =  bonobo.arg_type_from_gtype(gobject.TYPE_STRING)
        default = CORBA.Any (self.uri_type, "")
        bag.add ("URI", PackagePropertiesControl.PROP_URI,
                 self.uri_type, default, "URI of selected file", 0)

        self.set_properties (bag.corba_objref ())

        self.uri = ''

    def set_error (self, msg) :
        self.error_label.set_text (msg)
        self.notebook.set_current_page (1)

    def set_package_cb (self) :
        try :
            self.view.set_file (self.uri)
            self.notebook.set_current_page (0)
        except URINotFound :
            self.set_error ('The file could not be found')
        except URINotSupported :
            self.set_error ('This file does not belong to any package')
        except ximian_xmlrpclib.Fault, f :
            if (f.faultCode == fault.package_not_found) :
                self.set_error ('This file does not belong to any package')
            elif (f.faultCode == fault.permission_denied) :
                self.set_error ('You do not have permission to view package information')
            else :
                self.set_error (f.faultString)

        return FALSE

    def set_prop(self, pbag, arg, index, exc):
        if (index == PackagePropertiesControl.PROP_URI) :
            self.uri = arg.value ()
            gtk.idle_add (self.set_package_cb)
            
    def get_prop(self, pbag, typecode, index, exc):
        if (index == PackagePropertiesControl.PROP_URI) :
            return CORBA.Any (self.uri_type, self.uri)
        
def factory_fn(factory, iid):
    print "factory"
    return PackagePropertiesControl (ximian_xmlrpclib.Server ("/var/run/rcd/rcd", verbose=0))

    
def run_test (file) :
    try :
        server = ximian_xmlrpclib.Server ("/var/run/rcd/rcd", verbose=0)

        window = gtk.Window (gtk.WINDOW_TOPLEVEL)
        window.set_border_width (6)
        window.set_default_size (400, 400)

        try :
            view = PackagePropertiesView (server)
            view.set_file (file)
            view.show ()
        except URINotFound :
            print '%s does not exist'
            return
        except URINotSupported :
            print 'This file does not belong to any package'
            return
        except ximian_xmlrpclib.Fault, f :
            if (f.faultCode == fault.package_not_found) :
                print 'This file does not belong to any package'
            else :
                print f.faultString
            return
        
        window.add (view)
        window.connect ('delete_event', lambda *args : gtk.main_quit())
        window.show ()
        gtk.main ()
    except socket.error, e :
        error_dialog (None,
                      "There was an error connecting to the Red Carpet daemon")
        sys.exit (1)

def run_control () :
    bonobo.activate ()
    fact = bonobo.GenericFactory ("OAFIID:Nautilus_Package_Properties_View_Factory",
                                  factory_fn)
    bonobo.running_context_auto_exit_unref (fact)

    gtk.main ()


def run () :
    gtk.threads_init ()

    if len (sys.argv) > 1 :
        file = sys.argv[1]
    else :
        file = '/usr/bin/ghostscript'

    if narc_util.debugging ('page') :
        run_test (file)
    else :
        run_control ()
