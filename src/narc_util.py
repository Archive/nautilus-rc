import gtk
import os
import string
import ximian_xmlrpclib
import gobject

from narc_config import *

from gtk import TRUE, FALSE

class URINotSupported (Exception) :
    def __init__ (self) :
        Exception.__init__ (self)

class URINotFound (Exception) :
    def __init__ (self) :
        Exception.__init__ (self)

###
### KEEP THIS IN SYNC WITH RCD!
class fault:
    type_mismatch          = -501 # matches xmlrpc-c
    invalid_stream_type    = -503 # matches xmlrpc-c
    undefined_method       = -506 # matches xmlrpc-c
    permission_denied      = -600
    package_not_found      = -601
    package_is_newest      = -602
    failed_dependencies    = -603
    invalid_search_type    = -604
    invalid_package_file   = -605
    invalid_channel        = -606
    invalid_transaction_id = -607
    invalid_preference     = -608
    locked                 = -609
    cant_authenticate      = -610
    cant_refresh           = -611
    no_icon                = -612
    cant_activate          = -613
    not_supported          = -614

class StockImageButton (gtk.Button) :
    def __init__ (self, stockid, text) :
        gtk.Button.__init__ (self)
        
        align = gtk.Alignment (0.5, 0.5, 0.0, 0.0)
        align.show ()
        self.add (align)

        hbox = gtk.HBox (FALSE, 2)
        hbox.show ()
        align.add (hbox)
        
        image = gtk.Image ()
        image.set_from_stock (stockid, gtk.ICON_SIZE_BUTTON)
        image.show ()
        hbox.pack_start (image, FALSE, FALSE, 0)
        
        label = gtk.Label(text)
        label.set_use_underline (TRUE)
        label.set_mnemonic_widget (self)
        label.show ()
        hbox.pack_start (label, FALSE, FALSE, 0)

class GladeWidget (gtk.VBox) :
    def __init__ (self, filename, widget_name) :
        gtk.VBox.__init__ (self, FALSE, 0)
        self.xml = self.load_xml (filename, widget_name)

        self.pack_start (self.xml.get_widget (widget_name), TRUE, TRUE, 0)

    def create_size_group (self, names) :
        size_group = gtk.SizeGroup (gtk.SIZE_GROUP_HORIZONTAL)
        for name in names :
            size_group.add_widget (self.xml.get_widget (name))

    def load_xml (self, filename, root) :
        if os.path.exists ('./' + filename) :
            return gtk.glade.XML ('./' + filename, root)
        else :
            return gtk.glade.XML (narc_datadir + '/' + filename)
gobject.type_register (GladeWidget)

def extract_package (dep_or_package):
    # Check to see if we're dealing with package op structures or real
    # package structures.  Package structures won't have a "package" key.
    if dep_or_package.has_key("package"):
        return dep_or_package["package"]
    else:
        return dep_or_package

def extract_packages (dep_or_package_list):
    return map(lambda x:extract_package(x), dep_or_package_list)

def get_image_filename (filename) :
    return narc_datadir + "/" + filename

def error_dialog (widget, message) :
    if (widget) :
        toplevel = widget.get_toplevel ()
    else :
        toplevel = None

    dlg = gtk.MessageDialog (toplevel,
                             gtk.DIALOG_DESTROY_WITH_PARENT,
                             gtk.MESSAGE_ERROR, gtk.BUTTONS_OK,
                             message)

    dlg.connect ('response', lambda dlg, id: dlg.destroy ())
    dlg.show ()

def get_installed_package (server, name) :
    plist = server.rcd.packsys.search([["name", "is", name],
                                       ["installed", "is", "true"]])
    if (len (plist) > 0) :
        return plist[0]
    else :
        raise ximian_xmlrpclib.Fault (fault.package_not_found, 'This file does not belong to any installed package')

def remove_dups (list):
    for l in list:
        count = list.count(l)
        if count > 1:
            for i in range(1, count):
                list.remove(l)
 
    return list

def resolve_dependencies(server,
                         install_packages,
                         remove_packages,
                         extra_reqs):
    install_packages = remove_dups (install_packages)
    remove_packages = remove_dups (remove_packages)
    extra_reqs = remove_dups (extra_reqs)
 
    return server.rcd.packsys.resolve_dependencies (install_packages,
                                                    remove_packages,
                                                    extra_reqs)
 

def get_file_package_name (server, file) :
    if (not os.path.exists (file)) :
        raise URINotFound ()
    
    try :
        if (debugging ('oldrcd')) :
            raise ximian_xmlrpclib.Fault (fault.undefined_method,
                                          'Undefined method')

        package = server.rcd.packsys.find_package_for_file (file)
        package_name = package['name']
    except ximian_xmlrpclib.Fault, f :
        if (f.faultCode == fault.undefined_method) :
            # Old rcd, use rpm
            p = os.popen ("rpm -q --queryformat '%%{NAME}' -f %s" % (file))
            package_name = p.read();
            p.close()
            raise NotOwned 
            if (string.find (package_name, "is not owned") != -1) :
                raise ximian_xmlrpclib.Fault (fault.package_not_found,
                                              'This file does not belong to any package')
        else :
            raise
    

    return package_name

def get_uri_package_name (server, file) :
    if file[0] == '/' :
        return get_file_package_name (server, file)
    elif file[:7] != 'file://' :
        raise URINotSupported
    else :
        return get_file_package_name (server, file[7:])

debugging_flags = None
def debugging (flag) :
    global debugging_flags
    if (debugging_flags == None) :
        debugging_flags = {}

        str = os.getenv ('NAUTILUS_RC_TEST')
        if (str) :
            debug_flags = string.split (str, ',')
            for f in debug_flags :
                debugging_flags[f] = 1
                
    return debugging_flags.has_key (flag)
        
