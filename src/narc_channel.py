from narc_util import fault
from narc_util import get_image_filename
from narc_util import StockImageButton

import os
import gtk
import ximian_xmlrpclib

def get_channels (server) :
    return server.rcd.packsys.get_channels ()

def get_subscribed_channels (server) :
    channels = get_channels (server)
    subscribed_channels = []
    for c in channels :
        if c['subscribed'] :
            subscribed_channels.append (c)

    return subscribed_channels

def get_subscribed_channel_ids (server) :
    subscribed_channels = get_subscribed_channels (server)
    ids = []
    for c in subscribed_channels :
       ids.append (c['id'])
    return ids

cached_icons = {}

class Channel :
    def __init__ (self, server, id) :
        self.server = server

        self.channel = None

        for channel in get_channels (server) :
            if (channel['id'] == id) :
                self.channel = channel
            self.id = id

        if (self.channel == None) :
            raise ximian_xmlrpclib.Fault (fault.invalid_channel, 'Invalid channel id')
            
    def __getitem__ (self, key) :
        return self.channel[key]

    def get_channel_icon(self, width=0, height=0):
        if width > 0 and height > 0:
            key = "%s %d %d" % (str(self.id), width, height)
        else:
            key = str(self.id)
    
        if cached_icons.has_key(key):
            return cached_icons[key]

        pixbuf = None
        icon_data = None
        
        if width > 0 and height > 0:
            original = self.get_channel_icon()
            if (original) :
                pixbuf = original.scale_simple(width, height, gtk.gdk.INTERP_BILINEAR)
        else:
            try:
                icon_data = self.server.rcd.packsys.get_channel_icon(self.id)
            except ximian_xmlrpclib.Fault, f:
                if (f.faultCode == fault.no_icon) :
                    icon_data = None
                else :
                    raise

        if icon_data:
            loader = gtk.gdk.gdk_pixbuf_loader_new()
            loader.write(icon_data.data, len(icon_data.data))
            loader.close()
            pixbuf = loader.get_pixbuf()
        elif not pixbuf :
            filename = get_image_filename ("default-channel.png")

            if not os.path.exists (filename) :
                pixbuf = None
            else :
                pixbuf = gtk.gdk.pixbuf_new_from_file (filename)
            
        cached_icons[key] = pixbuf

        return pixbuf
