import narc_util

from narc_channel import Channel
from narc_util import fault
from narc_util import StockImageButton

import string
import sys
import time
import gobject
import gtk
import pango

from gtk import TRUE, FALSE
import ximian_xmlrpclib

MAX_LABEL_LEN = 20
WRAP_LABEL_LEN = 20

def transaction_status(message):
    messages = {"verify"       : ("Verifying"),
                "verify-undef" : ("Unable to verify package signature for"),
                "verify-nosig" : ("There is no package signature for"),
                "prepare"      : ("Preparing Transaction"),
                "install"      : ("Installing"),
                "remove"       : ("Removing"),
                "configure"    : ("Configuring")}
     
    status = string.split(message, ":", 1)
 
    m = messages[status[0]]
    if len(status) > 1:
        return m + " " + status[1]
    else:
        return m

class TransactionPreview (gtk.ScrolledWindow) :
    COLUMN_PIXBUF = 0
    COLUMN_PIXBUF_VISIBLE = 1
    COLUMN_NAME = 2
    COLUMN_VERSION = 3
    def __init__ (self,
                  server,
                  transaction) :
        gtk.ScrolledWindow.__init__ (self, None, None)

        self.server = server
        self.transaction = transaction
        
        self.set_policy (gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self.set_shadow_type (gtk.SHADOW_IN)

        self.tree = gtk.TreeView ()
        self.tree.show ()
        self.tree.set_headers_visible (FALSE)
        self.add (self.tree)

        self.store = gtk.ListStore (gtk.gdk.Pixbuf.__gtype__,
                                    gobject.TYPE_BOOLEAN,
                                    gobject.TYPE_STRING,
                                    gobject.TYPE_STRING)
        self.tree.set_model (self.store)

        column = gtk.TreeViewColumn ('package')
        
        renderer = gtk.CellRendererPixbuf ()
        column.pack_start (renderer, FALSE)
        column.add_attribute (renderer,
                              'pixbuf',
                              TransactionPreview.COLUMN_PIXBUF)
        column.add_attribute (renderer,
                              'visible',
                              TransactionPreview.COLUMN_PIXBUF_VISIBLE)

        renderer = gtk.CellRendererText ()
        column.pack_start (renderer, FALSE)
        column.add_attribute (renderer,
                              'markup',
                              TransactionPreview.COLUMN_NAME)

        renderer = gtk.CellRendererText ()
        column.pack_start (renderer, FALSE)
        column.add_attribute (renderer,
                              'text',
                              TransactionPreview.COLUMN_VERSION)

        self.tree.append_column (column)
        self.tree.get_selection().set_mode (gtk.SELECTION_NONE)

        self.add_package_list ('Requested Removals',
                               transaction.remove_packages)
        self.add_package_list ('Requested Installations',
                               transaction.install_packages)
        self.add_package_list ('Required Removals',
                               transaction.remove_deps)
        self.add_package_list ('Required Installations',
                               transaction.install_deps)
        
    def add_package_list (self, title, packages) :
        packages = narc_util.extract_packages (packages)
        
        if len (packages) > 0 :
            iter = self.store.append ()
            self.store.set (iter,
                            TransactionPreview.COLUMN_PIXBUF_VISIBLE,
                            FALSE,
                            TransactionPreview.COLUMN_NAME,
                            '<b>%s</b>' % (title))


            for package in packages :

                width, height = gtk.icon_size_lookup (gtk.ICON_SIZE_MENU)
                try :
                    channel = Channel (self.server, package['channel'])
                    pixbuf = channel.get_channel_icon (width, height)
                except ximian_xmlrpclib.Fault, f:
                    if (f.faultCode == fault.invalid_channel) :
                        pixbuf = None
                    else :
                        raise
                    
                
                iter = self.store.append ()
                self.store.set (iter,
                                TransactionPreview.COLUMN_PIXBUF,
                                pixbuf,
                                TransactionPreview.COLUMN_PIXBUF_VISIBLE,
                                TRUE,
                                TransactionPreview.COLUMN_NAME,
                                package['name'],
                                TransactionPreview.COLUMN_VERSION,
                                package['version'] + '-' + package['release'])


class TransactionPreviewDialog (gtk.Dialog) :
    def __init__ (self, parent, server, transaction, verb='_Install') :
        gtk.Dialog.__init__ (self)

        self.server = server

        if (parent) :
            self.set_transient_for (parent)
            self.set_position(gtk.WIN_POS_CENTER_ON_PARENT)
        
        self.set_default_size (350, 200)
        self.set_has_separator (FALSE)
        self.set_border_width (6)
        self.set_title ('Dependency Resolution')

        widget = TransactionPreview (self.server, transaction)
        widget.show ()
        self.vbox.pack_start (widget, TRUE, TRUE, 0)
        
        button = gtk.Button ('gtk-cancel')
        button.show ()
        button.set_use_stock (TRUE)
        self.add_action_widget (button, 0)

        button = StockImageButton ('gtk-execute', verb)
        button.show ()
        button.set_use_stock (TRUE)
        self.add_action_widget (button, 1)
        
class Transaction (gobject.GObject) :
    def __init__ (self, server,
                  install_packages, install_deps,
                  remove_packages, remove_deps) :
        gobject.GObject.__init__(self)

        self.install_packages = install_packages
        self.install_deps = narc_util.extract_packages (install_deps)
        self.remove_packages = remove_packages
        self.remove_deps = narc_util.extract_packages (remove_deps)

        self.info = {}
        self.info['elapsed_sec'] = 0
        self.info['remaining_sec'] = 0
        self.info['completed_size'] = 0
        self.info['total_size'] = 0
        self.info['percent_complete'] = 0
        self.info['state'] = 'downloading'
        self.info['active'] = 0
        self.info['error'] = None
        
        self.timeout_id = 0

        self.server = server

    def transaction_completed (self) :
        if self.timeout_id != 0 :
            gtk.timeout_remove (self.timeout_id)
        self.info['state'] = 'finished'
        self.emit ('changed')
        self.emit ('completed')

    def transaction_error (self, msg) :
        self.info['error'] = msg
        self.emit ('error', msg)

    def __getitem__ (self, key) :
        return self.info[key]

    def update_label_from_pending (self, pending) :
        if pending['messages'] :
            self.info['message'] = pending['messages'][-1]
            self.emit ('changed')

    def update_from_pending  (self, pending) :
        if pending['status'] == 'running' :
            self.info['active'] = TRUE
            self.info['elapsed_sec'] = pending.get ('elapsed_sec', 0)
            self.info['remaining_sec'] = pending.get ('remaining_sec', 0)
            self.info['completed_size'] = pending.get ('completed_size', 0)
            self.info['total_size'] = pending.get ('total_size', 0)
            self.info['percent_complete'] = pending.get ('percent_complete', 0.0)
        else :
            self.info['active'] = FALSE
        self.emit ('changed')

    def poll_transaction (self) :
        if self.download_id != -1 and not self.download_complete :
            
            pending = self.server.rcd.system.poll_pending (self.download_id)

            self.update_label_from_pending (pending)
            self.update_from_pending (pending)

            if pending['status'] == 'finished' :
                self.download_complete = 1
            elif pending['status'] == 'failed' :
                self.download_complete = 1
                self.transaction_error ('Could not download packages')
                self.transaction_completed ()
                return
        else :
            pending = self.server.rcd.system.poll_pending (self.transact_id)
            step_pending = self.server.rcd.system.poll_pending (self.step_id)

            self.update_label_from_pending (pending)
            self.update_from_pending (step_pending)

            if pending['status'] == 'finished' :
                self.transaction_completed ()
                return
            if pending['status'] == 'failed' :
                self.transaction_error (pending['error_msg'])
                self.transaction_completed ()
                return

    def poll_timeout (self) :
        self.poll_transaction ()
        if (self.info['state'] == 'finished') :
            self.timeout_id = 0
            return FALSE
        else :
            return TRUE

    def execute (self) :
        flags = 0
        if (narc_util.debugging ('dryrun')) :
            flags = 1

        self.download_complete = 0

        try :
            self.download_id, self.transact_id, self.step_id = \
                              self.server.rcd.packsys.transact (self.install_packages + self.install_deps,
                                                                self.remove_packages + self.remove_deps,
                                                                flags,
                                                                'Nautilus Red Carpet Client',
                                                                '0.0.1')
            self.timeout_id = gtk.timeout_add (200, self.poll_timeout)
        except ximian_xmlrpclib.Fault, f :
            self.transaction_error (f.faultString)
            self.transaction_completed ()

    def cancel (self) :
        if self.download_id == -1 or self.download_complete :
            # FIXME: maybe should throw an exception
            return

        ret = self.server.rcd.packsys.abort_download (self.download_id)

        if ret :
            self.transaction_error ('Download cancelled')
            self.transaction_completed ()

gobject.type_register (Transaction)
gobject.signal_new ('changed',
                    Transaction,
                    gobject.SIGNAL_RUN_LAST,
                    gobject.TYPE_NONE,
                    ())
gobject.signal_new ('completed',
                    Transaction,
                    gobject.SIGNAL_RUN_LAST,
                    gobject.TYPE_NONE,
                    ())
gobject.signal_new ('error',
                    Transaction,
                    gobject.SIGNAL_RUN_LAST,
                    gobject.TYPE_NONE,
                    (gobject.TYPE_STRING,))
 
class TransactionWindow (gtk.Window) :
    def __init__ (self, parent, transaction) :
        gtk.Window.__init__ (self, gtk.WINDOW_TOPLEVEL)

        self.window_parent = parent
        if (parent) :
            self.set_transient_for (parent)

        self.transaction = transaction
        
        self.set_border_width (12)

        main_box = gtk.VBox (FALSE, 10)
        self.add (main_box)

        hbox = gtk.HBox (FALSE, 10)
        self.image = gtk.Image ()
        hbox.pack_start (self.image, FALSE, FALSE, 0)

        text_box = gtk.VBox (FALSE, 6)

        self.title_label = gtk.Label ("")
        self.title_label.set_use_markup (TRUE)
        self.title_label.set_alignment (0, 0.5)
        self.set_title ("Updating packages")

        text_box.pack_start (self.title_label, FALSE, FALSE, 0)

        self.step_label = gtk.Label ("")
        self.step_label.set_alignment (0.0, 0.0)
        self.step_label.set_line_wrap (TRUE)
        (width, height) = self.calculate_required_text_size(MAX_LABEL_LEN)
        self.step_label.set_size_request(width, 3*height)
        self.line_height = height
 
        text_box.pack_start (self.step_label, FALSE, FALSE, 0)

        hbox.pack_start (text_box, TRUE, TRUE, 0)

        main_box.pack_start (hbox, FALSE, FALSE, 0)

        self.progress_bar = gtk.ProgressBar ()

        main_box.pack_start (self.progress_bar, FALSE, FALSE, 0)

        button_box = gtk.HButtonBox ()
        button_box.set_layout (gtk.BUTTONBOX_END)
        
        self.cancel_ok_button = gtk.Button (gtk.STOCK_CANCEL)
        self.cancel_ok_button.set_use_stock (TRUE)
        self.cancel_ok_button.connect ('clicked', self.cancel_ok_clicked_cb)
        button_box.add (self.cancel_ok_button)
                
        main_box.pack_start (button_box, FALSE, FALSE, 0)


        self.image_index = 0
        self.animation_timeout = 0
        self.iterate_animation ()

        main_box.show_all ()

        self.update_from_transaction ()
        transaction.connect ('changed',
                             self.transaction_changed_cb)
        transaction.connect ('error',
                             self.transaction_error_cb)

    def cancel_ok_clicked_cb (self, button) :
        if self.transaction['state'] == 'downloading':
            self.transaction.cancel ()
        elif self.transaction['state'] == 'finished' :
            self.destroy ()

    def get_success_message (self) :
        return 'The transaction has completed successfully'

    def get_finished_title (self) :
        return 'Transaction Complete'

    def get_transaction_message (self) :
        if self.transaction['state'] == 'downloading' :
            return 'Downloading packages'
        elif self.transaction['state'] == 'finished' :
            if self.transaction['error'] :
                return self.transaction['error']
            else :
                return self.get_success_message ()
        else :
            return transaction_status (self.transaction['message'])
            
    def update_from_transaction (self) :
        self.step_label.set_text (self.get_transaction_message ())
        self.start_spinning ()

        if (self.transaction['state'] == 'finished') :
            self.set_title (self.get_finished_title ())
            self.progress_bar.set_fraction (1.0)
            self.progress_bar.set_text("")
            self.cancel_ok_button.set_label (gtk.STOCK_CLOSE)
            self.cancel_ok_button.set_sensitive (TRUE)
            self.stop_spinning ()
        else :
            percent = self.transaction['percent_complete']

            if (self.transaction['active'] and percent > 0.0) :
                self.progress_bar.set_fraction (percent / 100.0)
                msg = "%.1f%%" % percent
                self.progress_bar.set_text (msg)
            else :
                self.progress_bar.set_text ("")
                self.progress_bar.pulse ()

                if self.transaction['state'] == 'downloading' :
                    self.cancel_ok_button.set_sensitive (TRUE)
                else :
                    self.cancel_ok_button.set_sensitive (FALSE)

    def transaction_changed_cb (self, transaction) :
        self.update_from_transaction ()

    def transaction_error_cb (self, transaction, msg) :
        narc_util.error_dialog (self, msg)

    def calculate_required_text_size(self, max_len):
        # "W" being the widest glyph.
        s = "W"*max_len
        layout = pango.Layout(gtk.Label("").get_pango_context())
        layout.set_text(s)
        (width, height) = layout.get_pixel_size()
 
        return (width, height)

    def iterate_animation (self) :
        self.image_index = self.image_index + 1
        if (self.image_index == 4) :
            self.image_index = 1
        filename = narc_util.get_image_filename ("spinning-rupert-%d.png" % (self.image_index))
        self.image.set_from_file(filename)
        return TRUE

    def start_spinning (self) :
        if (not self.animation_timeout) :
            self.animation_timeout = gtk.timeout_add (500,
                                                      self.iterate_animation)
    
    def stop_spinning (self) :
        if (self.animation_timeout) :
            self.image_index = 0
            self.iterate_animation ()
            gtk.timeout_remove (self.animation_timeout)
            self.animation_timeout = 0

    def set_title (self, title) :
        gtk.Window.set_title (self, title)
        self.title_label.set_markup ("<b><big>%s</big></b>" % title)
        self.position_window ()

    def set_label (self, label) :
        self.step_label.set_text (label)
        self.position_window ()
        
    def position_window(self):
        if self.window_parent:
            self.set_position(gtk.WIN_POS_CENTER_ON_PARENT)
        else:
            self.set_position(gtk.WIN_POS_CENTER)

class UpdateWindow (TransactionWindow) :
    def __init__ (self, parent, transaction) :
        TransactionWindow.__init__ (self, parent, transaction)

    def get_success_message (self) :
        return 'The update has completed successfully.'

    def get_finished_title (self) :
        return 'Update Complete'

class InstallationWindow (TransactionWindow) :
    def __init__ (self, parent, transaction) :
        TransactionWindow.__init__ (self, parent, transaction)

    def get_success_message (self) :
        return 'The installation has completed successfully.'

    def get_finished_title (self) :
        return 'Installation Complete'
